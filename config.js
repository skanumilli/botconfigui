// database configuration

const type = 'dev';

const dev = {
    app: {
        server: 'localhost',
        port: parseInt(process.env.port) || 3000
    },
    db: {
        host: process.env.DEV_DB_HOST || '192.168.100.7',
        port: parseInt(process.env.DEV_DB_PORT) || 1433,
        db_name: process.env.DEV_DB_NAME || 'RPSTest',
        user_id: 'sa-rpstest',
        password: '3!ndF!r3'
    }
};

const config = {
    dev: dev
};

module.exports = config[type]; 