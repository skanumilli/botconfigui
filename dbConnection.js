var express = require('express');
var config = require('./config');
var sql = require("mssql");

const dbConfig = {
    user: config.db.user_id,
    password: config.db.password,
    server: config.db.host,
    database: config.db.db_name
}

var sqlConPromise = sql.connect(dbConfig);
sql.on('error', err => {
    // ... error handler
    console.log("*************");
    console.log(err);
})

module.exports = sqlConPromise;
