$(document).ready(function () {
    
    $("#builderLink").addClass("activeLink");

    $("#loaderDiv").show();

    $("#keywordBuilder").jsGrid({
        width: "100%",
        height: "300",

        inserting: true,
        editing: true,
        sorting: true,
        selecting: true,
        autoload: true,

        controller: {
            loadData: function () {
                $("#loaderDiv").hide();
                return $.ajax({
                    type: "GET",
                    url: "/tableBuilder/keyword/data",
                    complete: function(){
                        $("#loaderDiv").hide();
                    }
                });
            },
            insertItem: function (item) {
                console.log(item);
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: "POST",
                        url: "/tableBuilder/keyword/data",
                        data: item,
                        success: function () {
                            alert("Successfully Inserted");
                        },
                        complete: function () {
                            resolve();
                            $("#loaderDiv").hide();
                        }
                    });
                });
            },
            updateItem: function (item) {
                console.log(item);
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: "PUT",
                        url: "/tableBuilder/keyword/data",
                        data: item,
                        complete: () => {
                            resolve();
                        }
                    });
                });
            },
            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/tableBuilder/keyword/data",
                    data: item
                });
            }
        },
        fields: [
            { name: "id", title: "ID",  type: "text", width: 60, validate: "required" },
            { name: "mailBotId", title: "Mail Bot ID", type: "text", width: 60, validate: "required" },
            { name: "searchTerm", title: "Search Term", type: "text", width: 60, validate: "required" },
            { type: "control" }
        ]
    });
});

    