var express = require('express');
var router = express.Router();
var sql = require('mssql'); 

// fetch data
router.get('/keyword/data', function (req, res, next) {

    let sqlStr = `select * from MailBotSearchTerms`;
    sqlConPromise
        .then(res => {
            console.log(sqlStr);
            return sql.query(sqlStr);
        })
        .then((result) => {
            res.setHeader('Content-Type', 'application/json');
            res.status(200).send(result['recordset']);
        })
        .catch(err => {
            console.log(err);
        })
});

// update data
router.put('/keyword/data', function (req, res, next) {

    console.log(req.body);
    let data = req.body,
        ID = data.ID,
        id = data.id,
        mailBotId = data.mailBotId,
        searchTerm = data.searchTerm;

        let sqlStr = `update MailBotSearchTerms set mailBotId = '${mailBotId}', searchTerm = '${searchTerm}', id = '${id}' where ID = ${ID}`;

    console.log(sqlStr);
    sqlConPromise
        .then(res => {
            console.log(sqlStr);
            return sql.query(sqlStr);
        })
        .then((result) => {
            console.log(result)
            res.setHeader('Content-Type', 'application/json');
            res.status(200).send("Success");
        })
        .catch(err => {
            console.log(err);
        })
});

// insert data
router.post('/keyword/data', function (req, res, next) {

    console.log(req.body);
    let data = req.body,
        id = data.id,
        mailBotId = data.mailBotId,
        searchTerm = data.searchTerm;
    
    let sqlStr = `Insert into MailBotSearchTerms (id, mailBotId, searchTerm) VALUES('${id}', '${mailBotId}', '${searchTerm}')`;
    sqlConPromise
        .then(res => {
            console.log(sqlStr);
            return sql.query(sqlStr); 
        })
        .then((result) => {
            console.log(result);
            res.setHeader('Content-Type', 'plain/text');
            res.status(200).send("Success");
        })
        .catch(err => {
            console.log(err); 
        })
});


// delete data
router.delete('/keyword/data', function (req, res, next) {
    console.log(req.body);
    let data = req.body,
        ID = data.ID; 

    let sqlStr = `Delete from MailBotSearchTerms where ID = ${ID}`;
    sqlConPromise
        .then(res => {
            console.log(sqlStr);
            return sql.query(sqlStr);
        })
        .then((result) => {
            //console.log(result)
            res.setHeader('Content-Type', 'plain/text');
            res.status(200).send("Successfully deleted");
        })
        .catch(err => {
            console.log(err);
        })
});



module.exports = router; 
